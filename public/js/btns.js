function open_catalog() {
    $('.favourites').css("display", "none");
    $('.catalog').css("display", "inline");
    localStorage.setItem('opened', 'catalog');
}

function open_favourites() {
    show_favourites();
    $('.catalog').css("display", "none");
    $('.favourites').css("display", "inline");
    localStorage.setItem('opened', 'favourites');
}

$(document).ready(function() {
    if (localStorage.getItem('opened') === 'favourites')
        open_favourites();
    else
        open_catalog();
});