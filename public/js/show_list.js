$(document).ready(function() {
    // Выводим список юзеров
    let to_html = [];
    to_html.push('<ul class="users">');
    $.getJSON('https://json.medrating.org/users/', function(data) {
        $.each(data, function(key, datum) {
            $.each(datum, function(key_of_datum, value) {
                // записываем id элемента в name
                if (key_of_datum === 'id') {
                    to_html.push('<li class="user" name="' + value);
                }
                // записываем name элемента в id и в содержимое тега
                if (key_of_datum === 'name') {
                    to_html.push('" id="' + value + '">' + value + '</li>');
                }
            });
        });
        to_html.push('</ul>');
        // Собираем массив
        to_html = to_html.join('');
        // Вывод
        $('.catalog').html(to_html);
    });
});

// Удаляем старый обработчик
$("div.catalog").unbind()
    // Выводим список альбомов по клику на юзера
    .on('click', 'li.user', function(event_user) {

        // Предотвращаем клик по дочерним элементам
        if (event_user.target.className !== 'user') {
            return;
        }

        // Сохраняем указатель на элемент
        let element = $(this);

        // Если это первый клик
        if ($(this).html() === $(this).attr('id')) {
            let to_html = [];
            to_html.push('<ul class="albums">');
            $.getJSON('https://json.medrating.org/albums?userId=' + $(this).attr('name'), function(data) {
                $.each(data, function(key, datum) {
                    $.each(datum, function(key_of_datum, value) {
                        // записываем id элемента в name
                        if (key_of_datum === 'id') {
                            to_html.push('<li class="album" name="' + value);
                        }
                        // записываем name элемента в id и в содержимое тега
                        if (key_of_datum === 'title') {
                            to_html.push('" id="' + value + '">' + value + '</li>');
                        }
                    });
                });
                to_html.push('</ul>');
                to_html = to_html.join('');
                $(element).html($(element).attr('id') + to_html);
            });

            // Удаляем старый обработчик
            $(".users").unbind()
                // Выводим список фото по клику на альбом
                .on('click', 'li.album', function(event_album) {
                    // Предотвращаем клик по дочерним элементам
                    if (event_album.target.className !== 'album') {
                        return;
                    }

                    let element = $(this);

                    // Если это первый клик
                    if ($(this).html() === $(this).attr('id')) {
                        let to_html = [];
                        to_html.push('<ul class="photos">');
                        $.getJSON('https://json.medrating.org/photos?albumId=' + $(this).attr('name'), function(data) {
                            $.each(data, function(key, datum) {
                                $.each(datum, function(key_of_datum, value) {
                                    // записываем id элемента в name и id
                                    if (key_of_datum === 'id') {
                                        to_html.push('<li class="photo" name="' + value + '">');
                                        to_html.push('<div class="column"><div class="row">');
                                        to_html.push('<div class="star" id="' + value + '" ');
                                    }
                                    // записываем title элемента в title и alt
                                    if (key_of_datum === 'title') {
                                        to_html.push('title="Добавить в избранное">&#9733;</div>');
                                        to_html.push('<div class="img">');
                                        to_html.push('<img class="photo" alt="' + value + '" title="' + value + '" ');
                                    }
                                    // Записываем url полноразмерной картинки в
                                    // кастомный тег data-full
                                    if (key_of_datum === 'url') {
                                        to_html.push('data-full="' + value + '" ');
                                    }
                                    // Записываем url маленькой картинки в src
                                    if (key_of_datum === 'thumbnailUrl') {
                                        to_html.push('src="' + value + '" /></li>');
                                        to_html.push('</div></div></div>');
                                    }
                                });
                            });
                            to_html.push('</ul>');
                            // Собираем массив
                            to_html = to_html.join('');
                            // Вывод
                            $(element).html($(element).attr('id') + to_html);
                            // Подсвечиваем нужные звёзды
                            update_favourites();
                        });
                    }
                    // Если это второй клик (список фото уже раскрыт)
                    else $(element).html($(element).attr('id'));
                });
        }
        // Если это второй клик (список альбомов уже раскрыт)
        else $(element).html($(element).attr('id'));
    })
