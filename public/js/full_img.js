$(document).ready(function() {

    // Вешаем обработчик клика по фото
    $('.main').on('click', '.photo', function(event_photo) {
        // Предотвращаем клик по дочерним элементам
        if (event_photo.target.className !== 'photo') {
            return;
        }

        // Вытаскиваем url полноразмерной картинки из data-full
        // и вставляем в url pop-up'а, показываем pop-up
        let url = $(this).attr('data-full');
        $('.full__div').html('<img alt="full" src="' + url + '" />');
        $('.fade').fadeIn();
        return false;
    });

    // Скрываем pop-up по клику на крестик
    $('.full__close').click(function() {
        $(this).parents('.fade').fadeOut();
        return false;
    });

    // Скрываем pop-up по клику на свободной области
    $('.fade').click(function(e) {
        if ($(e.target).closest('.full').length === 0) {
            $(this).fadeOut();
        }
    });
});