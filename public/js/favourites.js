function get_favourites() {
    return JSON.parse(localStorage.getItem('favourites') || '[]');
}

function save_favourites(new_favourites) {
    localStorage.setItem('favourites', JSON.stringify(new_favourites));
}

function add_to_favourites(new_favourite) {
    let favourites = get_favourites();
    favourites.push(new_favourite);
    save_favourites(favourites);
}

function remove_from_favourites(removable_favourite) {
    let favourites = get_favourites();
    favourites.splice($.inArray(removable_favourite, favourites), 1);
    save_favourites(favourites);
}

// Подсвечиваем нужные звёзды
function update_favourites() {
    let stars = $('.star');
    $.each(stars, function(key, star) {
        let favourites = get_favourites();
        let search = $.inArray($(star).attr('id'), favourites);
        if (search !== -1) {
            $(star).attr("title", "Удалить из избранного")
            $(star).css("color", "yellow");
        }
    });
}

$(document).ready(function() {
    // смена цвета звезды и удаление/сохранение избранного
    $("div.main").on('click', 'div.star', function() {
        if ($(this).attr("title") === "Добавить в избранное") {
            $(this).attr("title", "Удалить из избранного")
            $(this).css("color", "yellow");
            add_to_favourites($(this).attr('id'));
        } else {
            $(this).attr("title", "Добавить в избранное")
            $(this).css("color", "grey");
            remove_from_favourites($(this).attr('id'));
        }
    });
});

// Формируем список избранных картинок
function show_favourites() {
    let favourites = get_favourites();
    $('.favourites').html('');
    $.each(favourites, function(key, favourite) {
        let thumbnailUrl;
        let title;
        let url;
        //Поиск картинок для соответствующих "избранных"
        $.getJSON('https://json.medrating.org/photos?id=' + favourite, function(data) {
            let items = [];
            $.each(data, function(key, datum) {
                $.each(datum, function(key_of_datum, value) {
                    if (key_of_datum === 'thumbnailUrl')
                        thumbnailUrl = value;
                    if (key_of_datum === 'title')
                        title = value;
                    if (key_of_datum === 'url') {
                        url = value;
                    }
                });
            });

            // Формируем элемент списка избранных
            items.push('<div class="column"><div class="row">');
            items.push('<div class="star" id="' + favourite + '" title="Удалить из избранного">&#9733;</div>');
            items.push('<div class="img">')
            items.push('<img class="photo" alt="' + title + '" title="' + title);
            items.push('" src="' + thumbnailUrl + '" data-full="' + url + '" />');
            items.push('</div></div>');
            // Собираем массив
            items = items.join('');
            $('.favourites').append(items);
            // Подсвечиваем нужные звёзды
            update_favourites();
        });
    });
}